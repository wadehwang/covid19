#!/usr/bin/sh
accession=$1
curl 'https://www.ncbi.nlm.nih.gov/genomes/VirusVariation/vvsearch2/?q=${accession}&fq=%7B!tag=SeqType_s%7DSeqType_s:(%22Nucleotide%22)&fq=VirusLineageId_ss:(2697049)&fq=HostLineageId_ss:(9606)&cmd=download&sort=SourceDB_s%20desc,CreateDate_dt%20desc&dlfmt=fasta&fl=id,Definition_s,Nucleotide_seq' 2>err.txt 1>./${accession}.fasta 

s3cmd put ./${accession}.fasta s3://covid19/

#s3location= "s3://covid19/${accession}.fasta"
#echo $s3location

#> ${accession}.fasta
