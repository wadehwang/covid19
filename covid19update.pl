#!/usr/bin/perl
use strict;
use Data::Dumper;
use File::Basename;
use Cwd;

#die "halt";                                                                                                                                                                                

my $script_name=(split('\.',basename($0)))[0];
my $abs_path = Cwd::abs_path($0);
my $dirname  = File::Basename::dirname($abs_path);
my $lockfile="${dirname}/${script_name}.lock";
if(-e $lockfile){
    my $last_time=`cat $lockfile`;
    chomp $last_time;
    my $now_time=time();
    print "$last_time -> $now_time\n";
    my $time_diff=$now_time -$last_time;
    my $time_diff_hour=$time_diff/3600;
    print "$time_diff_hour\n";
    if($time_diff_hour < 2){
        print "Lockfile detected. Program might be running already.\n";
        exit;
    }else{
        #system("rm $lockfile");
        exit;
    }

}else{
    open OUT,">$lockfile" or die "can not open lockfile $lockfile";
    print OUT time();
    close OUT;
}



system ("curl 'https://www.ncbi.nlm.nih.gov/genomes/VirusVariation/vvsearch2/?q=*:*&fq=%7B!tag=SeqType_s%7DSeqType_s:(%22Nucleotide%22)&fq=VirusLineageId_ss:(2697049)&cmd=download&sort=SourceDB_s%20desc,CreateDate_dt%20desc&dlfmt=csv&fl=Accession:id,Release_Date:CreateDate_dt,Species:VirusSpecies_s,Genus:VirusGenus_s,Family:VirusFamily_s,Length:SLen_i,Nuc_Completeness:Flags_csv,Genotype:Serotype_s,Genome_Region:GenomeRegion_ss,Segment:Segment_s,Publications:PubMed_csv,Geo_Location:Country_s,Host:Host_s,Isolation_Source:Isolation_csv,Collection_Date:CollectionDate_s,BioSample:BioSample_s,GenBank_Title:Definition_s' 1> covid19update.nt.csv");

system("s3cmd put covid19update.nt.csv s3://covid19/nt/");
my @accesion = `cat covid19update.nt.csv |cut -d , -f 1`;
foreach my $accesion(@accesion)
{
    chomp $accesion;

    if($accesion eq "Accession"){next;}
    system ("curl 'https://www.ncbi.nlm.nih.gov/genomes/VirusVariation/vvsearch2/?q=$accesion&fq=%7B!tag=SeqType_s%7DSeqType_s:(%22Nucleotide%22)&fq=VirusLineageId_ss:(2697049)&fq=HostLineageId_ss:(9606)&cmd=download&sort=SourceDB_s%20desc,CreateDate_dt%20desc&dlfmt=fasta&fl=id,Definition_s,Nucleotide_seq' > $accesion.fasta");
    system("s3cmd put $accesion.fasta s3://covid19/nt/");
    system("rm $accesion.fasta");
}



system ("curl 'https://www.ncbi.nlm.nih.gov/genomes/VirusVariation/vvsearch2/?q=*:*&fq=%7B!tag=SeqType_s%7DSeqType_s:(%22Protein%22)&fq=VirusLineageId_ss:(2697049)&cmd=download&sort=SourceDB_s%20desc,CreateDate_dt%20desc&dlfmt=csv&fl=Accession:id,Release_Date:CreateDate_dt,Species:VirusSpecies_s,Genus:VirusGenus_s,Family:VirusFamily_s,Length:SLen_i,Genotype:Serotype_s,Genome_Region:GenomeRegion_ss,Segment:Segment_s,Publications:PubMed_csv,Protein:Protein_s,Geo_Location:Country_s,Host:Host_s,Isolation_Source:Isolation_csv,Collection_Date:CollectionDate_s,BioSample:BioSample_s,GenBank_Title:Definition_s'  1> covid19update.pt.csv");
system("s3cmd put covid19update.pt.csv s3://covid19/pt/");
my @accesion = `cat covid19update.pt.csv |cut -d , -f 1`;
foreach my $accesion(@accesion)
{
    chomp $accesion;

    if($accesion eq "Accession"){next;}
    system("mkdir -p protien ;mkdir -p coding_region");
    

    system ("curl 'https://www.ncbi.nlm.nih.gov/genomes/VirusVariation/vvsearch2/?q=$accesion&fq=%7B!tag=SeqType_s%7DSeqType_s:(%22Protein%22)&fq=VirusLineageId_ss:(2697049)&cmd=download&sort=Country_s%20asc,SourceDB_s%20desc,CreateDate_dt%20desc&dlfmt=fasta&fl=id,Definition_s,Protein_seq' > protien/$accesion.fasta");
    system("s3cmd put protien/$accesion.fasta s3://covid19/pt/protien/");
    system("rm protien/$accesion.fasta");

    system ("curl 'https://www.ncbi.nlm.nih.gov/genomes/VirusVariation/vvsearch2/?q=$accesion&fq=%7B!tag=SeqType_s%7DSeqType_s:(%22Protein%22)&fq=VirusLineageId_ss:(2697049)&cmd=download&sort=SourceDB_s%20desc,CreateDate_dt%20desc&dlfmt=fasta&fl=CdsLocation_s,Definition_s,Cds_seq' > coding_region/$accesion.fasta");
    system("s3cmd put coding_region/$accesion.fasta s3://covid19/pt/coding_region/");
    system("rm coding_region/$accesion.fasta");
}

system("rm $lockfile");
